drop table if exists users;
create table users
(
    userId int identity, /*identity 表示自增*/
    userName varchar (20),
    password varchar (20),
    flag int
);

drop table if exists category;
create table category
(
    categoryId int identity, /*identity 表示自增*/
    categoryName varchar (20)
);


drop table if exists goods;
create table goods
(
    goodsId int identity, /*identity 表示自增*/
    goodsName varchar (20),
    price double,
    address varchar (30),
    produceDate date ,
    picture varchar (100),
    categoryId int
);