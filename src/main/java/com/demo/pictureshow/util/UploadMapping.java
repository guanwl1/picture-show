package com.demo.pictureshow.util;

import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import java.io.File;
import java.net.URL;

@Configuration
public class UploadMapping implements WebMvcConfigurer {

    @Override
    public void addResourceHandlers(ResourceHandlerRegistry registry) {
        // 设置请求路径和文件物理路径的映射关系
        URL resource = this.getClass().getClassLoader().getResource("");
        registry.addResourceHandler("/up_file/**")
                .addResourceLocations(resource + File.separator);
    }
}
