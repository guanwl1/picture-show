package com.demo.pictureshow.controllers;

import com.demo.pictureshow.entity.Category;
import com.demo.pictureshow.service.CategoryService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.annotation.Resource;
import java.util.List;

@Controller
@RequestMapping("/admin/category")
public class AdminCategoryController {

    @Resource
    private CategoryService categoryService;

    @RequestMapping("/list")
    public String list(Model model){
        List<Category> categoryList = categoryService.list();
        model.addAttribute("categoryList",categoryList);
        return "admin/category_list";
    }

    @RequestMapping("/add_show")
    public String addShow(){
        return "admin/category_add";
    }

    @RequestMapping("/add_submit")
    public String addSubmit(Category category){
        categoryService.insert(category);
        return "redirect:admin/category/list";
    }

    @RequestMapping("/update_show")
    public String updateShow(Integer categoryId, Model model){
        Category category = categoryService.find(categoryId);
        model.addAttribute("category", category);
        return "admin/category_update";
    }

    @RequestMapping("/update_submit")
    public String updateSubmit(Category category){
        categoryService.update(category);
        return "redirect:admin/category/list";
    }


}