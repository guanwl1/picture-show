package com.demo.pictureshow.controllers;

import com.demo.pictureshow.entity.Category;
import com.demo.pictureshow.entity.Goods;
import com.demo.pictureshow.service.CategoryService;
import com.demo.pictureshow.service.GoodsService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.annotation.Resource;
import java.util.List;

@Controller
public class FrontHomeController {

    @Resource
    private CategoryService categoryService;

    @Resource
    private GoodsService goodsService;


    @RequestMapping("/")
    public String index(Model model){
        List<Category> categoryList = categoryService.list();
        List<Goods> goodsList = goodsService.findRandLimit(8);
        model.addAttribute("categoryList",categoryList);
        model.addAttribute("goodsList",goodsList);
        return "front/index";
//        return "欢迎访问我的项目<br/><a href='admin/login_show'>访问后台</a>";
    }


    @RequestMapping("/second")
    public String second(Integer categoryId, Model model){
        List<Category> categoryList = categoryService.list();
        List<Goods> goodsList = goodsService.findByCategoryId(categoryId);

        model.addAttribute("categoryList",categoryList);
        model.addAttribute("goodsList",goodsList);
        model.addAttribute("categoryId",categoryId);


        return "/front/second";

    }


}
