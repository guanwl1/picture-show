package com.demo.pictureshow.controllers;

import com.demo.pictureshow.entity.Users;
import com.demo.pictureshow.service.IUsersService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.annotation.Resource;
import javax.servlet.http.HttpSession;

@RequestMapping("/admin")
@Controller

public class AdminLoginController {

    @Resource
    private IUsersService usersService;

    @RequestMapping("login_show")
    public String loginShow() {
        return "admin/login";
    }

    @RequestMapping("login_submit")
    public String loginSubmit(Users user, HttpSession httpSession, Model model){
        // 根据用户名和密码进行登录校验
        Users users = usersService.adminLogin(user);
        if (users == null){
            model.addAttribute("err","用户名或密码不正确");
            return "admin/login";
        }
        httpSession.setAttribute("adminUser",user);
        return "admin/index";
    }
}
