package com.demo.pictureshow.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/admin")
public class AdminHomeController {

    @RequestMapping("/welcome")
    public String welcome(){
        return "admin/welcome";
    }
}
