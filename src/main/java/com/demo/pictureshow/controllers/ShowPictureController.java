package com.demo.pictureshow.controllers;

import com.demo.pictureshow.service.QueryPictureService;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.io.IOException;
import java.util.Objects;

@RestController
public class ShowPictureController {

    @Resource
    private QueryPictureService queryPictureService;

    @RequestMapping("hello")
    public String sayHello(){
        return "hello";
    }

    @RequestMapping("s")
    public byte[] showFile() throws IOException {
        return queryPictureService.queryPicture(Objects.requireNonNull(this.getClass().getClassLoader().getResource("application.properties")).getPath());
    }

    @RequestMapping("t")
    public String showFileString() throws IOException {
        byte[] bytes = queryPictureService.queryPicture("C:\\Users\\Nemo\\IdeaProjects\\picture-show\\src\\main\\resources\\application.properties");
        return new String(bytes);
    }
}
