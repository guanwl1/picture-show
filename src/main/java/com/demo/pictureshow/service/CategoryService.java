package com.demo.pictureshow.service;

import com.demo.pictureshow.entity.Category;

import java.util.List;

public interface CategoryService {

    List<Category> list();

    Category find(Integer categoryId);

    int insert(Category category);

    int update(Category category);

}
