package com.demo.pictureshow.service;

import com.demo.pictureshow.entity.Goods;

import java.util.List;

public interface GoodsService {
    List<Goods> list();
    Goods find(Integer goodsId);
    List<Goods> findRandLimit(Integer size);
    List<Goods> findByCategoryId(Integer categoryId);
    int insert(Goods goods);
    int update(Goods goods);
}
