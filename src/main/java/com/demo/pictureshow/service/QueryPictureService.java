package com.demo.pictureshow.service;

import org.springframework.stereotype.Service;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;

@Service
public class QueryPictureService {

    public byte[] queryPicture(String url) throws IOException {
        File file = new File(url);
        FileInputStream fileInputStream = new FileInputStream(file);
        int available = fileInputStream.available();
        byte[] bytes = new byte[available];
        int read = fileInputStream.read(bytes);
        System.out.println(read);
        return bytes;
    }
}
