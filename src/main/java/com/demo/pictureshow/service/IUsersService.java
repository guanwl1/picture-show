package com.demo.pictureshow.service;

import com.demo.pictureshow.entity.Users;

public interface IUsersService {

    Users adminLogin(Users users);
}
