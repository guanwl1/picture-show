package com.demo.pictureshow.service.impl;

import com.demo.pictureshow.entity.Goods;
import com.demo.pictureshow.mapper.GoodsMapper;
import com.demo.pictureshow.service.GoodsService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

@Service
public class GoodsServiceImpl implements GoodsService {

    @Resource
    private GoodsMapper goodsMapper;

    @Override
    public List<Goods> list() {
        return goodsMapper.findAll();
    }



    @Override
    public Goods find(Integer goodsId) {
        return goodsMapper.find(goodsId);
    }

    @Override
    public List<Goods> findRandLimit(Integer size) {
        return goodsMapper.findRandLimit(size);
    }

    @Override
    public List<Goods> findByCategoryId(Integer categoryId) {
        return goodsMapper.findByCategory(categoryId);
    }

    @Override
    public int insert(Goods goods) {
        return goodsMapper.doAdd(goods);
    }

    @Override
    public int update(Goods goods) {
        return goodsMapper.doUpdate(goods);
    }
}
