package com.demo.pictureshow.service.impl;

import com.demo.pictureshow.entity.Category;
import com.demo.pictureshow.mapper.CategoryMapper;
import com.demo.pictureshow.service.CategoryService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

@Service
public class CategoryServiceImpl implements CategoryService {

    @Resource
    private CategoryMapper categoryMapper;


    @Override
    public List<Category> list() {
        return categoryMapper.findAll();
    }

    @Override
    public Category find(Integer categoryId) {
        return categoryMapper.find(categoryId);
    }

    @Override
    public int insert(Category category) {
        return categoryMapper.doAdd(category);
    }

    @Override
    public int update(Category category) {
        return categoryMapper.doUpdate(category);
    }

}
