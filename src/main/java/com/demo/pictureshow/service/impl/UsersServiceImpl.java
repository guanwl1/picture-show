package com.demo.pictureshow.service.impl;

import com.demo.pictureshow.entity.Users;
import com.demo.pictureshow.mapper.UsersMapper;
import com.demo.pictureshow.service.IUsersService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

@Service
public class UsersServiceImpl implements IUsersService {

    @Resource
    private UsersMapper usersMapper;

    @Override
    public Users adminLogin(Users users) {
        return usersMapper.find(users);
    }
}
