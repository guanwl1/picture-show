package com.demo.pictureshow.mapper;

import com.demo.pictureshow.entity.Users;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface UsersMapper {
    Users find(Users users);

}
