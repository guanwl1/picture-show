package com.demo.pictureshow.mapper;

import com.demo.pictureshow.entity.Goods;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface GoodsMapper {

    List<Goods> findAll();

    List<Goods> findRandLimit(Integer size);

    // 根据分类查询商品
    List<Goods> findByCategory(Integer categoryId);

    Goods find(Integer goodsId);

    int doAdd(Goods goods);

    int doUpdate(Goods goods);
}
