package com.demo.pictureshow.mapper;

import com.demo.pictureshow.entity.Category;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface CategoryMapper {
    List<Category> findAll();

    Category find(Integer categoryId);

    int doAdd(Category category);

    int doUpdate(Category category);
}
